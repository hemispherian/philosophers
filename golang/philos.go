// Copyright 2015 Andras Becsi. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// philos.go
// The Hygienic Dining Philosophers Problem
// Chandy/Misra solution for the Dining Philosophers Problem
package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"golang.org/x/net/context"
)

// NumPhilosophers number of philosophers.
const NumPhilosophers int = 5

var logger = log.New(os.Stdout, "", 0)

// State type representing the activity state of a philosopher.
type State int

const (
	// Thinking state
	Thinking State = iota
	// Hungry state
	Hungry
	// Eating state
	Eating
)

// Request type representing the type of the request payload.
type Request int

const (
	// ForkRequest message
	ForkRequest Request = iota
	// ForkObject message
	ForkObject
)

// Fork struct representing a fork resource handle.
type Fork struct {
	owned        bool
	dirty        bool
	requestToken bool
}

// Result struct representing the summarized
// duration of philosopher's activities.
type Result struct {
	eaten   time.Duration
	thought time.Duration
	hungry  time.Duration
}

// Message struct representing a channel message
// for communication between philosophers.
type Message struct {
	senderID int
	payload  Request
}

// Mailman struct encapsulating a mailbox for a philosopher.
type Mailman struct {
	id      int
	mailbox chan Message
}

// Philosopher struct representing state of a philosopher.
type Philosopher struct {
	id      int
	state   State
	lefork  Fork
	rifork  Fork
	mailbox chan Message
	left    Mailman
	right   Mailman
	result  Result
}

// NewPhilosopher creates a new philosopher with ID, leftOwned and rightOwned fork.
func NewPhilosopher(id int, leftOwned, rightOwned bool) *Philosopher {
	return &Philosopher{
		id:      id,
		state:   Hungry,
		lefork:  Fork{owned: leftOwned, dirty: true, requestToken: !leftOwned},
		rifork:  Fork{owned: rightOwned, dirty: true, requestToken: !rightOwned},
		mailbox: make(chan Message, 2),
		result:  Result{},
	}
}

func (ph *Philosopher) String() string {
	return fmt.Sprintf("[%v]\nEating\t\t%v\nThinking\t%v\nHungry\t\t%v\n------", ph.id, ph.result.eaten, ph.result.thought, ph.result.hungry)
}

// Start starts philosopher with a shared context ctx,
// and WaitGroup to sync goroutine termination.
func (ph *Philosopher) Start(ctx context.Context, group *sync.WaitGroup) {
	eatingStarted := time.Now()
	thinkingStarted := time.Now()
	waitingStarted := time.Now()
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	thinkingDuration := time.Duration(random.Intn(2e3)) * time.Millisecond
	eatingDuration := time.Duration(random.Intn(2e3)) * time.Millisecond
	for {
		if ph.state != Eating {
			// Satisfy requests for dirty forks if not eating
			if ph.lefork.owned && ph.lefork.requestToken && ph.lefork.dirty {
				ph.left.mailbox <- Message{senderID: ph.id, payload: ForkObject}
				ph.lefork.owned = false
			}
			if ph.rifork.owned && ph.rifork.requestToken && ph.rifork.dirty {
				ph.right.mailbox <- Message{senderID: ph.id, payload: ForkObject}
				ph.rifork.owned = false
			}
		}
		switch ph.state {
		case Hungry:
			// Request forks if not available.
			if !ph.lefork.owned && ph.lefork.requestToken {
				ph.left.mailbox <- Message{senderID: ph.id, payload: ForkRequest}
				ph.lefork.requestToken = false
			} else if !ph.rifork.owned && ph.rifork.requestToken {
				ph.right.mailbox <- Message{senderID: ph.id, payload: ForkRequest}
				ph.rifork.requestToken = false
			}
			// Start eating if forks are available.
			if ph.lefork.owned && ph.rifork.owned {
				ph.result.hungry += time.Since(waitingStarted)
				ph.state = Eating
				ph.lefork.dirty = true
				ph.rifork.dirty = true
				eatingStarted = time.Now()
				eatingDuration = time.Duration(random.Intn(2e3)) * time.Millisecond
				log.Printf("[%02v] EATING    for\t%10v\t(waited for %v)\n", ph.id, eatingDuration, time.Since(waitingStarted))
			}
		case Thinking:
			// Transition to Hungry state if done thinking.
			if current := time.Since(thinkingStarted); current > thinkingDuration {
				ph.result.thought += current
				ph.state = Hungry
				waitingStarted = time.Now()
				log.Printf("[%02v] HUNGRY\n", ph.id)
			}
		case Eating:
			// Transition to Thinking state if done eating.
			if current := time.Since(eatingStarted); current > eatingDuration {
				ph.result.eaten += current
				ph.state = Thinking
				thinkingStarted = time.Now()
				thinkingDuration = time.Duration(random.Intn(2e3)) * time.Millisecond
				log.Printf("[%02v] THINKING  for\t%10v\n", ph.id, thinkingDuration)
			}
		}
		// Try twice to possibly receive messages from both neighbors.
		for i := 0; i < 2; i++ {
			select {
			case <-ctx.Done():
				// Decrement the WaitGroup counter when the goroutine is done.
				group.Done()
				return
			case msg := <-ph.mailbox:
				// Check our mailbox for messages from neighbors.
				switch msg.payload {
				case ForkRequest:
					if msg.senderID == ph.left.id {
						ph.lefork.requestToken = true
					}
					if msg.senderID == ph.right.id {
						ph.rifork.requestToken = true
					}
				case ForkObject:
					if msg.senderID == ph.left.id {
						ph.lefork.dirty = false
						ph.lefork.owned = true
					}
					if msg.senderID == ph.right.id {
						ph.rifork.dirty = false
						ph.rifork.owned = true
					}
				}
			default:
				// None of the above was available, sleep for a bit.
				time.Sleep(250 * time.Millisecond)
				continue
			}
		}
	}
}

func main() {
	quit := make(chan os.Signal)

	// When SIGINT or SIGTERM is caught write to the quit channel.
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	var philosophers [NumPhilosophers]*Philosopher

	for i := range philosophers {
		// Make the graph acyclic to prevent deadlocks.
		var leftOwned, rightOwned = i%2 == 0, i%2 == 0
		if i == NumPhilosophers-1 {
			// [0] owns this fork.
			rightOwned = false
		}
		philosophers[i] = NewPhilosopher(i, leftOwned, rightOwned)
	}

	// Set up the shared context and WaitGroup.
	ctx, cancel := context.WithCancel(context.Background())
	var group sync.WaitGroup
	// Increment the WaitGroup counter.
	group.Add(NumPhilosophers)

	for i := range philosophers {
		prev := i - 1
		next := (i + 1) % NumPhilosophers
		if prev < 0 {
			prev = NumPhilosophers - 1
		}

		// Set up mailboxes for neighbors.
		philosophers[i].left = Mailman{id: prev, mailbox: philosophers[prev].mailbox}
		philosophers[i].right = Mailman{id: next, mailbox: philosophers[next].mailbox}

		fmt.Print(philosophers[i].lefork.owned, "-")
		fmt.Print(" L[", i, "]R ")
		fmt.Print("-", philosophers[i].rifork.owned, "|")

		// Print the results after quit.
		defer log.Println(philosophers[i])
		go philosophers[i].Start(ctx, &group)
	}

	fmt.Println()
	<-quit

	cancel()

	group.Wait()
	fmt.Println("+++++")
}
