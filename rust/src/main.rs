// Copyright 2015 Andras Becsi. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// main.rs
// The Hygienic Dining Philosophers Problem
// Chandy/Misra solution for the Dining Philosophers Problem

use std::io;
use std::io::Write;
use std::thread;
use std::vec::Vec;
use std::sync::mpsc::{channel, Sender, Receiver, TryRecvError};

extern crate rand;
extern crate time;
use rand::Rng;
use time::{Duration,SteadyTime};

static NUMPHILOS: i32 = 5;

// Poor man's ternary operation.
macro_rules! cond_eq {
    ( $cond:expr, $t:expr, $f:expr ) => {
        match true {
            _ if $cond => {$t},
            _ => {$f}
        }
    };
}

macro_rules! try_or_dump {
    ( $ph:ident, $e:expr ) => {
        (match $e {
            Ok(e) => (e),
            Err(_) => {
                $ph.dump();
                return
            }
        })
    }
}

macro_rules! rand_ms {
    ( $limit:expr ) => {
        Duration::milliseconds(rand::thread_rng().gen::<i64>().abs() % $limit)
    }
}

macro_rules! to_ms {
    ( $num:expr ) => {
        match true {
            _ => { $num.num_milliseconds() }
        }
    }
}

macro_rules! now {
    () => {
        SteadyTime::now()
    }
}

// Philosophers state
enum State {
    Thinking,
    Hungry,
    Eating,
}

// Request message payload
enum Request {
    ForkRequest,
    ForkObject,
    Quit,
}

// Message struct for communication between philosophers
struct Message {
    sender_id:  i32,
    payload: Request,
}

impl Message {
    fn new(id: i32, msg: Request) -> Message {
        Message {
            sender_id: id,
            payload: msg
        }
    }
}

// Fork representing a handle for a fork
struct Fork {
    owned:         bool,
    request_token: bool,
    dirty:         bool,
}

// Result for summarized duration of philosopher's activity
struct Result {
    eaten:   i64,
    thought: i64,
    hungry:  i64,
}

impl Result {
    fn new() -> Result {
        Result { eaten:0, thought:0, hungry:0 }
    }
}

// Mailman encapsulating a sender for a philosopher
struct Mailman {
    id: i32,
    mailbox: Sender<Message>,
}

// Philosopher representing state of a philosopher
struct Philosopher {
    id: i32,
    state: State,
    lefork: Fork,
    rifork: Fork,
    left: Mailman,
    right: Mailman,
    sender: Sender<Message>,
    mailbox: Receiver<Message>,
    result: Result,
}

// Philosopher implementation
impl Philosopher {
    fn new(id: i32, left_owned: bool, right_owned: bool) -> Philosopher {
        let (t, r) = channel::<Message>();
        Philosopher {
            id: id,
            state: State::Hungry,
            lefork: Fork { owned: left_owned, request_token: !left_owned, dirty: false },
            rifork: Fork { owned: right_owned, request_token: !right_owned, dirty: false },
            left: Mailman {id: -1, mailbox: t.clone()},
            right: Mailman {id: -1, mailbox: t.clone()},
            sender: t,
            mailbox: r,
            result: Result::new(),
        }
    }
    fn channel(&self) -> Sender<Message> {
        // Cloning a sender for neighbors.
        self.sender.clone()
    }
    fn dump(&self) {
        io::stdout().flush().unwrap();
        thread::sleep_ms(1000);
        println!("{}\neaten=   {}ms\nthought= {}ms\nhungry= {}ms\n-----"
        , self.id, self.result.eaten, self.result.thought, self.result.hungry);
    }
    fn start(&mut self) {
        let mut eating_started = now!();
        let mut thinking_started = now!();
        let mut waiting_started = now!();
        let mut thinking_duration = rand_ms!(2000);
        let mut eating_duration = rand_ms!(2000);
        loop {
            match self.state {
                State::Hungry | State::Thinking => {
                    // Not eating
                    if self.lefork.owned && self.lefork.request_token && self.lefork.dirty {
                        try_or_dump!(self, self.left.mailbox.send(Message::new(self.id, Request::ForkObject)));
                        self.lefork.owned = false;
                    }
                    if self.rifork.owned && self.rifork.request_token && self.rifork.dirty {
                        try_or_dump!(self, self.right.mailbox.send(Message::new(self.id, Request::ForkObject)));
                        self.rifork.owned = false;
                    }
                },
                _ => {}
            }
            match self.state {
                State::Hungry => {
                    if !self.lefork.owned && self.lefork.request_token {
                        try_or_dump!(self, self.left.mailbox.send(Message::new(self.id, Request::ForkRequest)));
                        self.lefork.request_token = false;
                    } else if !self.rifork.owned && self.rifork.request_token {
                        try_or_dump!(self, self.right.mailbox.send(Message::new(self.id, Request::ForkRequest)));
                        self.rifork.request_token = false;
                    }
                    if self.lefork.owned && self.rifork.owned {
                        let current = now!() - waiting_started;
                        self.result.hungry += to_ms!(current);
                        self.state = State::Eating;
                        self.lefork.dirty = true;
                        self.rifork.dirty = true;
                        eating_started = now!();
                        eating_duration = rand_ms!(2000);
                        println!("[{}] EATING   for {}ms\t(waited for {}ms)", self.id, to_ms!(eating_duration), to_ms!(current));
                    }
                },
                State::Thinking => {
                    let current = now!() - thinking_started;
                    if current > thinking_duration {
                        self.result.thought += to_ms!(current);
                        self.state = State::Hungry;
                        waiting_started = now!();
                        println!("[{}] HUNGRY", self.id);
                    }
                },
                State::Eating => {
                    let current = now!() - eating_started;
                    if current > eating_duration {
                        self.result.eaten += to_ms!(current);
                        self.state = State::Thinking;
                        thinking_started = now!();
                        thinking_duration = rand_ms!(2000);
                        println!("[{}] THINKING for {}ms", self.id, to_ms!(thinking_duration));
                    }
                }
            }
            for _ in 0..2 {
                match self.mailbox.try_recv() {
                    Err(TryRecvError::Empty) => {
                        // No message received, sleep for a bit.
                        thread::sleep_ms(250);
                        continue
                    },
                    Err(TryRecvError::Disconnected) => {
                        self.dump();
                        return;
                    },
                    Ok(msg) => {
                        match msg.payload {
                            Request::ForkRequest => {
                                if msg.sender_id == self.left.id {
                                    self.lefork.request_token = true;
                                }
                                if msg.sender_id == self.right.id {
                                    self.rifork.request_token = true;
                                }
                            },
                            Request::ForkObject => {
                                if msg.sender_id == self.left.id {
                                    self.lefork.dirty = false;
                                    self.lefork.owned = true;
                                }
                                if msg.sender_id == self.right.id {
                                    self.rifork.dirty = false;
                                    self.rifork.owned = true;
                                }
                            },
                            Request::Quit => {
                                self.dump();
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}

fn main() {
    let mut philosophers: Vec<Philosopher> = Vec::with_capacity(NUMPHILOS as usize);
    let mut channels: Vec<Sender<Message>> = Vec::with_capacity(NUMPHILOS as usize);

    for i in 0..NUMPHILOS {
        // Make the graph acyclic to prevent deadlocks.
        let left_owned: bool = i%2 == 0;
        // Even ids own the forks, correct last right ownership
        // for odd number of philosophers (fork owned by [0]).
        let right_owned: bool = cond_eq!(i == NUMPHILOS-1, false, i%2 == 0);

        philosophers.push(Philosopher::new(i, left_owned, right_owned));
        // Keep sender channels to be able to send termination message.
        channels.push(philosophers[i as usize].channel());
    }

    // Wire up neighbor channels.
    for i in 0..NUMPHILOS {
        let prev = cond_eq!(i-1 < 0, NUMPHILOS-1, i-1);
        let next = (i+1)%NUMPHILOS;
        let index = i as usize;
        philosophers[index].left = Mailman {
            id: prev,
            mailbox: philosophers[prev as usize].channel()
        };
        philosophers[index].right = Mailman {
            id: next,
            mailbox: philosophers[next as usize].channel()
        };
        print!("{}-", philosophers[index].lefork.owned);
        print!(" L[{}]R ", index);
        print!("-{}|", philosophers[index].rifork.owned);
    }
    print!("\n");

    // Use a consuming iterator, which makes it possible to
    // move each philosopher to its respective thread.
    let handles: Vec<_> = philosophers.into_iter().map(|mut ph| {
        thread::spawn(move || {
            ph.start();
        })
    }).collect();

    let mut inp = String::new();
    io::stdin().read_line(&mut inp).unwrap();
    println!("DONE");

    for c in channels {
        c.send(Message::new(-1, Request::Quit)).unwrap();
    }

    for h in handles {
        h.join().unwrap();
    }
}
